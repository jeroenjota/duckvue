# duckvue

## Project setup

### Start the server:
```
cd ./api
npm install
nodemon server.js
```

## Start client
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Open browser
```
goto 'http://localhost/8080'
```
