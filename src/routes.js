// routes.js

import Home from './components/Home.vue'
import Newtour from './components/Newtour.vue'
import Ducksedit from './components/Ducksedit.vue'
import Instructions from './components/Instructions.vue'
import Error from './components/404.vue'

const routes = [
    { path: '/', component: Home, name: 'home'},
    { path: '/newtour', component: Newtour, name: 'newtour'},
    { path: '/instructions', component: Instructions, name: 'instructions'},
    { path: '/ducksedit', component: Ducksedit, name: 'ducksedit'},
    { path: '/404', component: Error, name: '404'},
]

export default routes
