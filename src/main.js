import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import routes from './routes'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

Vue.use(Router)
Vue.use(Notifications, { velocity })

import VueAxios from 'vue-axios'
import axios from 'axios'

Vue.use(require('vue-moment'))
Vue.use(VueAxios, axios)

const router = new Router( { mode: 'history', routes })
new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
