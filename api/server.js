// server.js

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const pdf = require('html-pdf')
const pug = require('pug')
const PORT = 4000;
// const path = require('path')
const cors = require('cors');
const fs = require('fs')
const path = require('path')
const moment = require('moment')


moment.locale('nl')
app.use(cors());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));


app.listen(PORT, function() {
  console.log('Server is running on Port:', PORT);
});
// Puppeteer
// wordt NIET gebruikt, werkt niet met Bootstrap
// async function createPDF(html, fileName) {
//   const browser = await puppeteer.launch();
//   const page = await browser.newPage();
//   await page.setContent(html)
//   await page.waitFor(300)
//   // await page.emulateMedia('print')
//   await page.pdf({
//     path: fileName + ".pdf",
//     format: "A4",
//     printBackground: true,
//     margin: {
//       top: "1cm",
//       right: "1cm",
//       bottom: "1cm",
//       left: "1cm"
//     }
//   })
//   await browser.close()
// }
//

app.post('/api/v1/print', function(req, res) {
  let data = req.body
  //    res.send(req. body)
  let filePath = "./public/pdf/"
  let ducks = data.ducksChosen
  const custName = data.customerName || "Onbekende klant"
  console.log("Genereer formulieren voor " + custName + " ( " + moment(data.datum).format('d-MM-YYYY') + " )")
  if (data.printAnswerForms) {

    // generate AntwoordForms
    ducks.forEach(function(duck, index) {

      var teamnr = index * 1 + 1

      // render antwform to pdf with data
      let fileName = filePath + custName + "_form_" + teamnr + ".pdf"
      let antwTempl = pug.compileFile("./views/antwform.pug")

      let htmlString = antwTempl({
        data: data,
        moment: moment,
        duck: duck,
        teamnr: teamnr,
      })
      // old-fashioned pdf creation
      pdf.create(htmlString).toFile(fileName, function(err, res){
        if (err) return console.log(err)
        console.log(res)
      })
    })
  }
  // generate admin forms
  // scoreforms
  if (data.gameCount > 0) {
    for (var count = 1; count <= data.gameCount; count++) {
      console.log("Genereer scoreformulier " + count)
      let fileName = filePath + custName + "_score_form_" + count + ".pdf"
      let antwTempl = pug.compileFile("./views/scoreform.pug")
      let htmlString = antwTempl({
        data: data,
        moment: moment
      })

      pdf.create(htmlString).toFile(fileName, function(err, res) {
        if (err) return console.log(err)
        console.log(res)
      })

    }
  }
  if (data.totalForm) {
    console.log("Genereer totaal formulier")
    let fileName = filePath + custName + "_total_form" + ".pdf"
    let antwTempl = pug.compileFile("./views/totalform.pug")
    let htmlString = antwTempl({
      data: data,
      moment: moment
    })

    pdf.create(htmlString).toFile(fileName, function(err, res) {
      if (err) return console.log(err)
      console.log(res)
    })
  }
  if (data.telList) {
    console.log("Genereer telefoonlijst")
    let fileName = filePath + custName + "_tellijst" + ".pdf"
    let antwTempl = pug.compileFile("./views/tellist.pug")
    let htmlString = antwTempl({
      data: data,
      moment: moment
    })

    pdf.create(htmlString).toFile(fileName, function(err, res) {
      if (err) return console.log(err)
      console.log(res)
    })
  }
  if (data.A4) {
    for (var i = 1; i <= data.ducksChosen.length; i++) {
      console.log("Print A4 vel voor team " + i)
    }
  }
  res.sendStatus(200);
})

app.get('/api/v1/ducks', function(req, res) {
	var pad = path.join(__dirname, 'public', 'data', 'eendenlijst.txt')
	fs.readFile(pad, function(error, duckBuffer) {
		if (error) {
			console.error('Kon bestand niet lezen', error)
			return res.status(500).json({error: error.message})
		}
		var ducks = duckBuffer.toString()
		// Split op newline, sorteer abc, maak nieuw array met de map function. De functie wordt aangeroepen voor elk item
		// in een array.
		var json = ducks.split('\r\n').sort().map(function(duck) {
			// Deze syntax pakt item 0 en 1 uit een array en stopt ze in de name en isofix vars
			// isofix is undefined als hij niet bestaat
			var [ name, isofix ] = duck.split(';') // duck = "eend naam;[isofix]"
			isofix = Boolean(isofix)
			// Deze syntax creëert een object met: { name: "Eend naam", isofix: false/true }
			return { name, isofix }
		})
		// Respond met array
		res.json(json)
	})
})
